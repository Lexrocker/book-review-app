from django.shortcuts import render, redirect

from reviews.models import Review
from reviews.forms import ReviewForm

# Create your views here.

def list_reviews(request):
    reviews = Review.objects.all()
    context = {

        "reviews": reviews,

    }

    return render(request, "reviews/main.html", context)

def create_review(request): #declare a function named create_review, which has a single impt parameter that contains the html request
    form = ReviewForm() #create a new form for the request
    if request.method == "POST" : #if the request is a post then continue below
        form = ReviewForm(request.POST) # create a new form with the data in the HTTP POST
        if form.is_valid(): # test if the data is valid
            form.save()     # save the data
            return redirect("reviews_list") # return to main page of reveiws

    context = { # create a context dictionary

        "form" : form, # add a form to the dictionary
    }

    return render(request, "reviews/create.html", context) # render the template and retern it to Django

def review_detail(request, id):
    review = Review.objects.get(id=id)
    context = {

        "review": review,
    }

    return render(request, "reviews/detail.html", context)